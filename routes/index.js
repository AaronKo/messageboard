var express = require('express');
var router = express.Router();
var User = require('../models/user');


/* GET home page. */
router.get('/', function(req, res, next) {
    if(req.user){
        res.redirect('/forum');
    }
    else{
        res.render('index', {});
    }
});

router.get('/logout', function(req, res, next){
    req.session.reset();
    res.redirect('/');
});

router.post('/login', function(req, res){
  User.findOne({UserName: req.body.UserName}, function(err, user){
    if(!user){
        res.redirect("/");
    }
    else{
      if (req.body.Password === user.Password){
          console.log(user);
        req.session.user = user;
        res.redirect('/forum');
      }
      else{
          res.redirect("/");
      }
    }
  });
});

router.post('/register', function(req, res){
    console.log(req.body);
    User.findOne({UserName: req.body.UserName}, function(err, doc){
       if (err) throw (err);
        if (doc){
            res.status(400).send("Failed: Username already registered.");
        }
        else{
            var newUser = new User(req.body);
            newUser.save(function(err, user){
                console.log(req.body);
                req.session.user = req.body;
                res.redirect('/forum');
            });
        }
    });
});

module.exports = router;
