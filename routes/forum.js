var express = require('express');
var router = express.Router();
var Post = require('../models/postModel');
var Message = require('../models/postMessageModel');

var moment = require('moment');

function requireLogin (req, res, next) {
    if (!req.user) {
        console.log("no user");
        res.redirect('/');
    } else {
        next();
    }
};

router.get('/', requireLogin, function(req, res, next){
    Post.find({deleted: false}, function(err, posts){
        console.log("alltheposts" + posts);
        if (err) throw (err);
        for (var i=0; i<posts.length; i++){
            posts[i].date = moment(posts[i].createdAt).format('MMMM DD YYYY, h:mm:ss a');
            posts[i].from = "("+moment(posts[i].createdAt).fromNow()+")";
            posts[i].permission = false;
            if(req.user._id.toString() == posts[i].user._id.toString()){
                console.log(req.user._id == posts[i].user._id);
                console.log("user!!");
                posts[i].permission=true;
            }
        }
        res.render('forum/index', {
            user: req.user,
            posts: posts
        });
    });
});

router.get('/newPost', requireLogin, function(req, res, next){
    res.render('forum/newPost');
});

router.post('/newPost', requireLogin, function(req, res, next){
    var newPost = new Post({
        title: req.body.title,
        user: req.user
    });
        newPost.save(newPost, function(err){
        if(err) throw (err);
        var newMessage = Message({
            _post: newPost._id,
            message: req.body.message,
            user: req.user
        });
        newMessage.save(function(err){
            if(err) throw (err);
            res.redirect('/forum');
        });
    });
});

router.get('/:id', requireLogin, function(req, res, next){
    Message.find({_post: req.params.id, deleted: false}, function(err, messages){
        for (var i=0; i<messages.length; i++){
            messages[i].date = moment(messages[i].createdAt).format('MMMM DD YYYY, h:mm:ss a');
            messages[i].from = "("+moment(messages[i].createdAt).fromNow()+")";
            messages[i].permission = false;
            if(req.user._id.toString() == messages[i].user._id.toString()){
                console.log(req.user._id == messages[i].user._id);
                console.log("user!!");
                messages[i].permission=true;
            }
        }
        Post.findOne({_id: req.params.id}, function(err, post){
            res.render('forum/post', {
                messages: messages,
                user: req.user,
                postid: req.params.id,
                post: post
            });
        });
    });
});

router.post('/:id', requireLogin, function(req, res, next){
    console.log(req.body);
    var newMessage = new Message({
        _post: req.params.id,
        message: req.body.message,
        user: req.user
    });
    newMessage.save(function(err){
        if (err) throw (err);
        res.redirect('/forum/'+req.params.id);
    });
});

router.get('/deleteMessage/:id', requireLogin, function(req, res, next){
    Message.findOne({_id: req.params.id}, function(err, message){
        if(message){
            var postid = message._post;
            if(message.user._id.toString() === req.user._id.toString()){
                Message.update({_id: req.params.id}, {deleted: true},function(err){
                    res.redirect("/forum/"+postid);
                });
            }
            else{
                res.redirect('/forum');
            }
        }
        else{
            res.redirect('/forum')
        }
    });
});

router.post('/editMessage/:id', requireLogin, function(req, res, next){
    console.log(req.body.message);
    Message.findOne({_id: req.params.id}, function(err, message){
        if(message.user._id.toString() === req.user._id.toString()){
            message.message = req.body.message;
            Message.update({_id: req.params.id}, message, function(err){
                res.status(204).json({yo: "yo"});
            });
        }
        else{
            res.redirect("/forum")
        }
    });
});

router.get('/deletePost/:id', requireLogin, function(req, res, next){
    Post.findOne({_id: req.params.id}, function(err, post){
        if(post){
            if(post.user._id.toString() === req.user._id.toString()){
                Post.update({_id: req.params.id}, {deleted: true},function(err){
                    res.redirect("/forum/");
                });
            }
            else{
                res.redirect('/forum');
            }
        }
        else{
            res.redirect('/forum')
        }
    });
});

module.exports = router;
