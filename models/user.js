var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var User = mongoose.model('user',
    {
        FullName: String,
        UserName: String,
        Password: String,
    });

module.exports = User;