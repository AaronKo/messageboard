/**
 * Created by Aaron on 5/4/2016.
 */
$(function(){
    var editFunc = function(){
        var id = $(this).parent().find("input").attr("value");
        console.log(id);
        var textEdit = $(this).parent().find("p");
        var text = textEdit.text();
        var area = $("<textarea></textarea>").append(text);
        area.attr("class", "form-control");
        textEdit.replaceWith(area);
        area.focus();
        area.blur(function(){
            console.log("blurred");
            text = area.val();
            $.ajax({
                url: "/forum/editMessage/"+id,
                method: "Post",
                data: {message: text},
                success: function(){
                    area.replaceWith($("<p></p>").append(text));
                }
            })
        })
    };

    var $window = $(window),
        $stickyEl = $('#the-sticky-div'),
        elTop = $stickyEl.offset().top;

    $window.scroll(function() {
        $stickyEl.toggleClass('sticky', $window.scrollTop() > elTop);
    });

    $("#divContainer").on("click", ".editButton", editFunc);
});
