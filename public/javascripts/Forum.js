/**
 * Created by Aaron on 5/6/2016.
 */
$(function(){
    var $window = $(window),
        $stickyEl = $('#the-sticky-div'),
        elTop = $stickyEl.offset().top;

    $window.scroll(function() {
        $stickyEl.toggleClass('sticky', $window.scrollTop() > elTop -20);
    });
});

